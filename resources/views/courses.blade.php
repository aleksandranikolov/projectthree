@extends('layouts.template')

@section('content')

@include('partials.navigation')

<div class="container-fluid bg-light pt-5">
    <div class="row ml-5">
        <div class="col-1">
            <img src="{{$languageObject->icon}}" class="logo-image-cousres ml-5">
        </div>

        <div class="col-6 ml-3">
            <h4>{{$languageObject->name}} Tutorials and Courses </h4>
            <p>Learn {{$languageObject->name}} online from the best {{$languageObject->name}} tutorials submitted & voted by the programming community.
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 ml-5 border border-primary rounded filter-div pb-4">
            <p class="py-2 border-bottom border-info font-weight-bold">Filter Courses</p>

            <p class="font-weight-bold">Type of course</p>
            <div class="form-check mt-2">
                <input class="form-check-input filters" <?php if ($type == 'free') echo "checked"; ?> type="checkbox" value="free" id="free" name="type">
                <label class="form-check-label" for="free">Free ({{ $freeCount }})</label>
            </div>
            <div class="form-check mt-2">
                <input class="form-check-input filters " type="checkbox" value="paid" id="paid" <?php if ($type == 'paid') echo "checked"; ?> name="type">
                <label class="form-check-label" for="paid">Paid ({{ $paidCount }})</label>
            </div>

            <p class="font-weight-bold mt-3">Medium</p>
            <div class="form-check mt-2">
                <input class="form-check-input filters" type="checkbox" {{ ($medium == 'video') ? 'checked' : '' }} value="video" id="video" name="medium">
                <label class="form-check-label" for="video">Video ({{ $videoCount }})</label>
            </div>
            <div class="form-check mt-2">
                <input class="form-check-input filters" type="checkbox" {{ ($medium == 'book') ? 'checked' : '' }} value="book" id="book" name="medium">
                <label class="form-check-label" for="book">Book ({{ $bookCount }})</label>
            </div>

            <p class="font-weight-bold mt-3">Level</p>
            <div class="form-check">
                <input class="form-check-input filters" type="checkbox" {{ ($level =='beginer') ? 'checked' : '' }} value="beginer" id="beginer" name="level">
                <label class="form-check-label" for="beginer">Beginer ({{ $beinerCount }})</label>
            </div>
            <div class="form-check mt-2">
                <input class="form-check-input filters" type="checkbox" {{ ($level == 'advanced') ? 'checked' : '' }} value="advanced" id="advanced" name="level">
                <label class="form-check-label" for="advanced">Advanced ({{ $advancedCount }})</label>
            </div>

            <p class="font-weight-bold mt-3">Language</p>

            @foreach($courseLanguages as $key => $courseLanguage)
            <div class="form-check mt-2">
                <input class="form-check-input filters" type="checkbox" {{ ($naturalLanguage == $courseLanguage) ? 'checked' : '' }} value="{{ $courseLanguage }}" id="{{ $courseLanguage }}" name="language">
                <label class="form-check-label" for="language">{{ $courseLanguage }}</label>
            </div>
            @endforeach
        </div>

        <div class="col-lg-8 bg-light">
            <div class="row">
                @foreach($courses as $course)
                <div class="col-lg-4 col-sm-12 mt-2 mb-2">
                    <div class="card courses-card">
                        <div class="card-body">
                            <div class="shadow-sm p-2 mb-2 bg-white rounded">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="font-weight-bold"> {{ ($course->course_name) }} </h5>
                                    </div>
                                    <div class="col-4">
                                        <a class="button tutorial-link" href=" {{ ($course->link) }} ">view <i class="far fa-hand-pointer"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <span class="badge badge-pill text-white badge-info ml-3">{{ ($course->medium) }}</span>
                                    <span class="badge badge-pill text-white badge-info ml-3">{{ ($course->language) }}</span>
                                </div>
                                <div class="col-6">
                                    @if($course->type =='free')
                                    <span class="badge badge-pill text-white badge-success ml-3">{{ ($course->type) }}</span>
                                    @endif

                                    <span class="badge badge-pill text-white badge-info ml-2">{{ ($course->level) }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-9">
                                    <p class="mt-3">By: {{ $course->user->name }}</p>
                                    <p>{{ ($course->created_at->format('M d Y G:i')) }}</p>
                                </div>
                                <div class="col-3">
                                    <div class="reviewd">R</div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            Likes
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-6 offset-6 text-center mt-4">
            {{ $courses->links() }}
        </div>
    </div>


</div>

<div class="row mx-5 pb-5">
    <div class="col-12">
        <span>You might also be interested in:</span>
    </div>
    
        @foreach($randoms as $random)
        <div class="col-4 mt-2">
            <a href="{{ route('courses', ['language' => $random['id']]) }}">
                <div class="card">
                    <div class="card-body">
                        <span>{{ $random->name }}</span>
                        <span><img src="{{ asset($random->icon) }}" class="logo-image"></span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    
</div>


@endsection