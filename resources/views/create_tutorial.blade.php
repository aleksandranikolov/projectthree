@extends('layouts.template')
@section('content')
<div class="container-fluid w-50 pt-5 text-center">
    <div class="topic">
        <h3>Submit a Tutorial/Course</h3>
        <p>Feel free to submit tutorials in any language.</p>
        <small>Paste the link below:</small>
    </div>
    <form id="create_tutorial" action="{{ route('create-tutorial') }}" method="POST">
        @csrf

        <div class="form-group pt-5">
            <input type="text" class="form-control" id="name" placeholder="Tutorial name" name="course_name">
            <p class="bg-light mt-3 py-1">Insert name of the tutorial</small>
        </div>

        <div class="form-group pt-5">
            <input type="url" class="form-control" id="url-field" placeholder="URL of the tutorial" name="link">
            <p class="bg-light mt-3 py-1">Tell us more about this 'tutorial/course' (optional)</small>
        </div>


        @if ($categories->count() > 0)
        <div class="form-group">
            <label for="categories">Category:</label>
            <select name="categories[]" id="categories" class="form-control categories-selector" multiple="multiple">
                @foreach ($categories as $category)
                <option value="{{ $category->id }}" )>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        @endif


        <div class="form-group">
            <label>Tags:</label>
            <label><input type="checkbox" name="type" id="free" value="free"> Free</label>
            <label><input type="checkbox" name="type" value="paid"> Paid</label>

            <label class="pl-4"><input type="checkbox" name="medium" value="video"> Video</label>
            <label><input type="checkbox" name="medium" value="book"> Book</label>
        </div>

        <div class="form-group">
            <label>This course is for:</label>
            <label><input type="radio" name="level" value="beginer">Beginer</label>
            <label><input type="radio" name="level" value="advanced">Advanced</label>
        </div>


        <div class="form-group">
            <label>Language:</label>
            <input type="text" name="language" class="form-control" id="language" placeholder="Insert language...">
        </div>


        <button type="submit" class="btn btn-primary btn-block font-weight-bold">Submit</button>

    </form>
    <hr>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



<script>
    $(document).ready(function() {
        $(".categories-selector").select2({
            placeholder: "Python, Angular, etc",
            allowClear: true,
            minimumInputLength: 1,
            maximumSelectionSize: 1,
            width: '100%',
        });
    });
</script>
@endsection