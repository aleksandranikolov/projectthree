@extends('layouts.template')

@section('content')

@include('partials.navigation')


<div class="container-fluid main-div pt-5">
    <h2 id="title">Find the Best Programming Courses & Tutorials</h2>
    <input id="myFilter" onkeyup="myFunction()" class="form-control navbar-search-input js-navbar-search-input nav-input text-secondary js-filter-topics" type="text" placeholder=" Search for the language you want to learn: Python, Javascript,...">
   

    <div class="row cards-row pt-5" id="myProducts">
        @foreach($languages as $language)
        <div class="col-lg-4 py-1 card-container">
            <a href="{{ route('courses', ['language' => $language['id']]) }}">
                <div class="card">
                    <div class="card-body">
                        <img src="{{$language['icon'] }}" class="logo-image">
                        <span class="card-title">{{$language['name']}}</span>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>

</div>

<script>
    function myFunction() {
        let input, filter, cards, cardContainer, title, i;

        input = document.getElementById("myFilter");
        filter = input.value.toUpperCase();
        cardContainer = document.getElementById("myProducts");
        cards = cardContainer.getElementsByClassName("card-container");

        for (i = 0; i < cards.length; i++) {
            title = cards[i].querySelector(".card-title");
            if (title.innerText.toUpperCase().indexOf(filter) > -1) {
                cards[i].style.display = "";
            } else {
                cards[i].style.display = "none";
            }
        }
    }
</script>

@endsection