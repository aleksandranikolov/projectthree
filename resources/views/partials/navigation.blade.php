<!-- Navigation -->
<nav class="navbar navbar-expand-lg pl-5">
    <a class="navbar-brand" href="{{ url('/home') }}">
        <img src="{{ asset('logos/brainster-symbol-small.png') }}">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto page-navigation-links">
            <li class="nav-item "> <a class="nav-link" href="{{ url('/home') }}"> <span><img class="nav-icon " src="https://hackr.io/assets/images/nav-icons/programming.svg" alt="Programming"></span> Programming </a> </li>
            <li class="nav-item "> <a class="nav-link" href="#"> <span><img class="nav-icon " src="https://hackr.io/sites/data-science/icon?ver=1571096418" alt="Data Science"></span> Data Science </a> </li>
            <li class="nav-item  active"> <a class="nav-link" href="#"> <span><img class="nav-icon " src="https://hackr.io/sites/devops/icon?ver=1576396835" alt="DevOps"></span> DevOps </a> </li>
            <li class="nav-item "> <a class="nav-link" href="#"> <span><img class="nav-icon " src="https://hackr.io/sites/design/icon?ver=1571096431" alt="Design"></span> Design </a> </li>
        </ul>

        <div class="right">
            <form class="form-inline float-left">
                <img class="icon color-filter" src="https://hackr.io/assets/images/header-icons/search-header.svg" width="17" height="17">
                <input class="form-control mr-sm-2 global-search" type="search" placeholder="Search for topics" aria-label="Search">
            </form>

            <div class="navbar-collapse collapse js-nav-icon-hidden" id="navbarCollapse">
                <ul class="nav navbar-nav">
                    @if (Route::has('login'))
                    <div class="top-right links">
                        @if(!Auth::check())
                        <li class="nav-item float-left mt-2 submit-tutorial register-login-navigation">
                            <a title="Submit a tutorial" style="cursor: pointer" class="nav link" data-toggle="modal" data-target="#registerModal" data-message="You need to Sign up to submit a Tutorial" data-trigger="submit_tutorial_btn"> <img class="icon-plus color-filter" src="https://hackr.io/assets/images/header-icons/add.svg" width="10" alt="submit"> <span> Submit a tutorial</span> </a>
                        </li>
                        <li class="nav-item float-left register-login-navigation">
                            <a class="nav-link" style="cursor: pointer" data-toggle="modal" data-target="#registerModal">{{ __('Sign Up / Sign In') }}</a>
                        </li>
                        @else
                        <li class="nav-item float-left mt-1 register-login-navigation">
                            <a href="{{ route('show') }}" title="Submit a tutorial" class="submit-tutorial pt-2" data-message="You need to Sign up to submit a Tutorial" data-trigger="submit_tutorial_btn"> <img class="icon-plus color-filter" src="https://hackr.io/assets/images/header-icons/add.svg" width="10" alt="submit"> <span> Submit a tutorial</span> </a>
                        </li>

                        <li class="nav-item float-left register-login-navigation">
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ 'Logout' }}
                            </a>
                        </li>
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
            @endif
        </div>
        @endif
    </div>
    </div>
</nav>