
jQuery(document).ready(function ($) {
    $('#login-form').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serializeArray();
        $(".invalid-feedback").children("strong").text("");
        $("#login-form input").removeClass("is-invalid");
        $.ajax({
            method: "POST",
            headers: {
                Accept: "application/json"
            },
            url: window.loginRoute,
            data: formData,
            success: () => {
                window.location.assign("/")
            },
            error: (response) => {
                alert('Wrong credentials');
            }
        })
    });
});
