$(function () {
    $('#submitTutorialForm').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serializeArray();
        $(".invalid-feedback").children("strong").text("");
        $("#submitTutorialForm input").removeClass("is-invalid");
        $.ajax({
            method: "POST",
            headers: {
                Accept: "application/json"
            },
            url: window.submitTutorialRoute,
            data: formData,
            success: () => {
                window.location.assign("/")
            },
            error: (response) => {
                alert('Error in submiting, check input fields and try again.');
                console.log('error');
            }
        })
    });
})