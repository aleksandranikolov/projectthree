$(function () {
    $('#registerForm').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serializeArray();
        $(".invalid-feedback").children("strong").text("");
        $("#registerForm input").removeClass("is-invalid");
        $.ajax({
            method: "POST",
            headers: {
                Accept: "application/json"
            },
            url: window.registerRoute,
            data: formData,
            success: () => {
                window.location.assign("/")
            },
            error: (response) => {
                alert('Error registering, check input fields and try again.');
                console.log('error');
            }
        })
    });
})