
jQuery(document).ready(function($) {
    function addToQueryString(param, value) {
        if ('URLSearchParams' in window) {
            var searchParams = new URLSearchParams(window.location.search);
            searchParams.set(param, value);
            window.location.search = searchParams.toString();
        }
    }

    $('.filters').on('click', function () {
        let value = $(this).attr('value');

        if ($(this).is(':checked') == false) {
            value = "";
        }

        addToQueryString($(this).attr('name'), value);
    })
})