<?php

use App\Language;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $languages = Language::all()->toArray();
//     // dd($languages);
//     return view('welcome', compact('languages'));
    
    
// });
Route::get('/', 'HomeController@index')->name('welcome');

Auth::routes();
Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')
    ->name('social.login');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')
    ->name('social.callback');

    Route::get('/home', 'HomeController@index')->name('home');

Route::get('/courses/{language}', 'CourseController@index')->name('courses');
Route::get('/courses/{field}', 'HomeController@index')->name('field');

// Insert Tutorial
Route::get('/show-tutorial', 'CourseController@show')->name('show');
Route::post('/insert-tutorial', 'CourseController@store')->name('create-tutorial');

