<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $filable = [
        'name', 'internal_name', 'icon', 'category'
    ];

    public function courses() {
        return $this->belongsToMany(Course::class);
    }

    public function fields() {
        return $this->belongsTo(Field::class);
    }
}


