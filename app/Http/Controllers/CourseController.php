<?php

namespace App\Http\Controllers;

use App\Course;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function index($language, Request $request)
    {
        $type = $request->get('type');
        $medium = $request->get('medium');
        $level = $request->get('level');
        $naturalLanguage = $request->get('language');
        $languageObject = Language::find($language);
       
        $courses = Course::with(['languages:name', 'user'])->get();
        $courses = Course::with(['languages'])->whereHas('languages', function ($query) use ($language) {
            $query->where('languages.id', '=', $language);
        });
        


        if ($type) {
            $courses = $courses->where('type', '=', $type);
        }

        if ($medium) {
            $courses = $courses->where('medium', '=', $medium);
        }

        if ($level) {
            $courses = $courses->where('level', '=', $level);
        }

        if ($naturalLanguage) {
            $courses = $courses->where('language', '=', $naturalLanguage);
        }

        $courses = $courses->paginate(10);

        $paid = Course::where('type', '=', 'paid')->get();
        $paidCount = $paid->count();

        $free = Course::where('type', '=', 'free')->get();
        $freeCount = $free->count();

        $video = Course::where('medium', '=', 'video')->get();
        $videoCount = $video->count();

        $book = Course::where('medium', '=', 'book')->get();
        $bookCount = $book->count();


        $beginer = Course::where('level', '=', 'beginer')->get();
        $beinerCount = $beginer->count();


        $advanced = Course::where('level', '=', 'advanced')->get();
        $advancedCount = $advanced->count();

        $courseLanguages = Course::all('language')->unique('language')->pluck('language')->toArray();
      
        
        $randomLanguages = Language::all();
        $randoms = $randomLanguages->random(6);
    
      
        return view('courses', compact('type', 'medium', 'level', 'naturalLanguage', 'courseLanguages', 'languageObject', 'courses', 'paidCount', 'freeCount', 'videoCount', 'bookCount', 'beinerCount', 'advancedCount', 'randoms'));
    }

    public function show()
    {
        $categories = Language::all();
        $courses = Course::all();

        return view('create_tutorial', compact('courses', 'categories'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        // dd($user->toArray());


        $course = new Course();
        $course->course_name = $request->get('course_name');
        $course->link = $request->get('link');
        $course->type = $request->get('type');
        $course->medium = $request->get('medium');
        $course->level = $request->get('level');
        $course->language = $request->get('language');
        $course->user_id = $user->id;
        $course->save();

        foreach ($request->get('categories') as $category) {
            $course->languages()->attach($category);
        }
        return redirect()->route('welcome');
    }
}
