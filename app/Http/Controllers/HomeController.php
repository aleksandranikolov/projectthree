<?php

namespace App\Http\Controllers;

use App\Course;
use App\Language;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $languages = Language::all()->toArray();
        return view('welcome', compact('languages'));
    }
}
