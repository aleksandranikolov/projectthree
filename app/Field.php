<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public function languages() {
        return $this->hasMany(Language::class);
    }
}
