<?php

use App\Field;
use Illuminate\Database\Seeder;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fieldOne = new Field;
        $fieldOne->field_internal_name = 'programming';
        $fieldOne->field_name = 'Programming';
        $fieldOne->save();

        $fieldTwo = new Field;
        $fieldTwo->field_internal_name = 'data_science';
        $fieldTwo->field_name = 'Data Science';
        $fieldTwo->save();

        $fieldThree = new Field;
        $fieldThree->field_internal_name = 'devops';
        $fieldThree->field_name = 'DevOps';
        $fieldThree->save();

        $fieldFour = new Field;
        $fieldFour->field_internal_name = 'design';
        $fieldFour->field_name = 'Design';
        $fieldFour->save();
    }
}
