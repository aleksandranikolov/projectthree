<?php

use App\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courseOne = new Course;
        $courseOne->course_name = 'Front End Web Developer Nanodegree (udacity.com)';
        $courseOne->link = 'https://www.udacity.com/course/front-end-web-developer-nanodegree--nd0011?irclickid=wmQULAU8oxyLUAAwUx0Mo3EJUkEXSSwVBQd9Vs0&irgwc=1&utm_source=affiliate&utm_medium=&aff=1419154&utm_term=&utm_campaign=__&utm_content=&adid=786224';
        $courseOne->type = 'paid';;
        $courseOne->medium = 'video';
        $courseOne->level = 'Beginner';
        $courseOne->language = 'English';
        $courseOne->user_id = 1;
        $courseOne->save(); 
    }
}
