<?php

use App\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $python = new Language();
        $python->name = 'Python' ;
        $python->internal_name = 'python';
        $python->icon = 'https://hackr.io/tutorials/python/logo-python.svg';
        $python->category = 'programming';
        $python->field_id ='1';
        $python->save();

        $javaScript = new Language();
        $javaScript->name = 'JavaScript';
        $javaScript->internal_name = 'java_script';
        $javaScript->icon = 'https://hackr.io/tutorials/javascript/logo-javascript.svg';
        $javaScript->category = 'programming';
        $javaScript->field_id ='1';
        $javaScript->save();

        $html = new Language();
        $html->name = 'HTML 5';
        $html->internal_name = 'html';
        $html->icon = 'https://hackr.io/tutorials/html-5/logo-html-5.svg';
        $html->category = 'programming';
        $html->field_id ='1';
        $html->save();

        $java = new Language();
        $java->name = 'Java';
        $java->internal_name = 'java';
        $java->icon = 'https://hackr.io/tutorials/java/logo-java.svg';
        $java->category = 'programming';
        $java->field_id ='1';
        $java->save();

        $css = new Language();
        $css->name = 'CSS';
        $css->internal_name = 'css';
        $css->icon = 'https://hackr.io/tutorials/css/logo-css.svg';
        $css->category = 'programming';
        $css->field_id ='1';
        $css->save();

        $cPlus = new Language();
        $cPlus->name = 'C++';
        $cPlus->internal_name = 'c_plus';
        $cPlus->icon = 'https://hackr.io/tutorials/c-plus-plus/logo-c-plus-plus.svg';
        $cPlus->category = 'programming';
        $cPlus->field_id ='1';
        $cPlus->save();

        $data = new Language();
        $data->name = 'Data Structures and Algorithms';
        $data->internal_name = 'data';
        $data->icon = 'https://hackr.io/tutorials/data-structures-algorithms/logo-data-structures-algorithms.svg';
        $data->category = 'programming';
        $data->field_id ='1';
        $data->save();

        $react = new Language();
        $react->name = 'React';
        $react->internal_name = 'react';
        $react->icon = 'https://hackr.io/tutorials/react/logo-react.svg';
        $react->category = 'programming';
        $react->field_id ='1';
        $react->save();

        $node = new Language();
        $node->name = 'Node.js';
        $node->internal_name = 'node';
        $node->icon = 'https://hackr.io/tutorials/node-js/logo-node-js.svg';
        $node->category = 'programming';
        $node->field_id ='1';
        $node->save();

        $c = new Language();
        $c->name = 'C';
        $c->internal_name = 'c';
        $c->icon = 'https://hackr.io/tutorials/c/logo-c.svg';
        $c->category = 'programming';
        $c->field_id ='1';
        $c->save();

    }
}
